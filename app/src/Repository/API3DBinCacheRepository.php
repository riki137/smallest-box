<?php

namespace App\Repository;

use App\Entity\API3DBinCache;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method API3DBinCache|null find($id, $lockMode = null, $lockVersion = null)
 * @method API3DBinCache|null findOneBy(array $criteria, array $orderBy = null)
 * @method API3DBinCache[]    findAll()
 * @method API3DBinCache[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class API3DBinCacheRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, API3DBinCache::class);
    }
}
