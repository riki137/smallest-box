<?php

namespace App\Repository;

use App\Entity\PackingBin;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PackingBin|null find($id, $lockMode = null, $lockVersion = null)
 * @method PackingBin|null findOneBy(array $criteria, array $orderBy = null)
 * @method PackingBin[]    findAll()
 * @method PackingBin[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PackingBinRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PackingBin::class);
    }
}
