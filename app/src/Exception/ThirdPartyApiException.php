<?php

namespace App\Exception;

class ThirdPartyApiException extends \Exception
{
    public function __construct(?\Throwable $previous = null)
    {
        parent::__construct('Third party API connection has failed', 0, $previous);
    }
}
