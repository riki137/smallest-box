<?php


namespace App\Exception;


class NoBinFoundException extends \Exception
{

    /**
     * NoBinFoundException constructor.
     */
    public function __construct(\Throwable $previous = null)
    {
        parent::__construct('No fitting bin found', 0, $previous);
    }
}
