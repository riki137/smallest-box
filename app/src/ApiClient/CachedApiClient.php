<?php


namespace App\ApiClient;


use App\Entity\API3DBinCache;
use App\Exception\ThirdPartyApiException;
use App\Repository\API3DBinCacheRepository;
use Doctrine\ORM\EntityManagerInterface;
use GuzzleHttp\Psr7\Response;
use JsonException;
use Psr\Http\Message\ResponseInterface;

final class CachedApiClient implements ApiClientInterface
{
    private const CACHE_STATUS = 'status';
    private const CACHE_HEADERS = 'headers';
    private const CACHE_BODY = 'body';

    private ApiClient $apiClient;
    private API3DBinCacheRepository $cacheRepository;
    private EntityManagerInterface $entityManager;

    public function __construct(
        ApiClient $apiClient,
        API3DBinCacheRepository $cacheRepository,
        EntityManagerInterface $entityManager
    ) {
        $this->apiClient = $apiClient;
        $this->cacheRepository = $cacheRepository;
        $this->entityManager = $entityManager;
    }

    /**
     * @throws JsonException
     * @throws ThirdPartyApiException
     */
    public function post(string $url, ?array $data = null): ResponseInterface
    {
        $cacheKey = self::cacheKey($url, $data);

        $cachedData = $this->cacheRepository->findOneBy(['request' => $cacheKey]);
        if ($cachedData instanceof API3DBinCache) {
            $responseData = json_decode($cachedData->getResponse(), true, 512, JSON_THROW_ON_ERROR);
            return new Response(
                $responseData[self::CACHE_STATUS],
                $responseData[self::CACHE_HEADERS],
                $responseData[self::CACHE_BODY]
            );
        }

        $response = $this->apiClient->post($url, $data);
        $responseData = [
            self::CACHE_STATUS => $response->getStatusCode(),
            self::CACHE_HEADERS => $response->getHeaders(),
            self::CACHE_BODY => $response->getBody()->getContents(),
        ];
        $cacheEntity = new API3DBinCache($cacheKey, json_encode($responseData, JSON_THROW_ON_ERROR));
        $this->entityManager->persist($cacheEntity);
        $this->entityManager->flush();

        $response->getBody()->rewind();

        return $response;
    }

    /**
     * @throws JsonException
     */
    private static function cacheKey(string $url, ?array $data = null)
    {
        return json_encode([$url, $data], JSON_THROW_ON_ERROR);
    }
}
