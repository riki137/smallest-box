<?php

namespace App\ApiClient\Schema;

use Swaggest\JsonSchema\Schema;
use Swaggest\JsonSchema\Structure\ClassStructure;

final class PackingItem extends ClassStructure
{
    public int $id;
    public int $width;
    public int $height;
    public int $depth;
    public int $weight;

    public static function setUpProperties($properties, Schema $ownerSchema): void
    {
        $properties->id = Schema::integer();
        $properties->width = Schema::integer();
        $properties->height = Schema::integer();
        $properties->depth = Schema::integer();
        $properties->weight = Schema::integer();

        $ownerSchema->required = [
            self::names()->id,
            self::names()->width,
            self::names()->height,
            self::names()->depth,
            self::names()->weight,
        ];
    }
}
