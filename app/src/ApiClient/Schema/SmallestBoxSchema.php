<?php

namespace App\ApiClient\Schema;

use Swaggest\JsonSchema\Schema;
use Swaggest\JsonSchema\Structure\ClassStructure;

final class SmallestBoxSchema extends ClassStructure
{
    /**
     * @var PackingItem[]
     */
    public array $items = [];

    public static function setUpProperties($properties, Schema $ownerSchema): void
    {
        $properties->items = Schema::arr();
        $properties->items->items = PackingItem::schema()->nested();
        $ownerSchema->required = [self::names()->items];
    }

}
