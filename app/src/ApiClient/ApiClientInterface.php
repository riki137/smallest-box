<?php


namespace App\ApiClient;


use App\Exception\ThirdPartyApiException;
use Psr\Http\Message\ResponseInterface;

interface ApiClientInterface
{
    public const ENDPOINT_PACK = 'packer/pack';

    /**
     * @param string $url
     * @param array  $data
     * @return ResponseInterface
     * @throws ThirdPartyApiException
     */
    public function post(string $url, array $data = []): ResponseInterface;
}
