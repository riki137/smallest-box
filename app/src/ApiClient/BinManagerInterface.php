<?php

namespace App\ApiClient;

use App\Entity\PackingBin;

interface BinManagerInterface
{
    /**
     * @return PackingBin[]
     */
    public function getAllBins(): array;
}
