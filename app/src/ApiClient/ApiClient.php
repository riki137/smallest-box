<?php

namespace App\ApiClient;

use App\Exception\ThirdPartyApiException;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Psr\Http\Message\ResponseInterface;

final class ApiClient implements ApiClientInterface
{
    private const QUERY_USERNAME = 'username';
    private const QUERY_API_KEY = 'api_key';
    private Client $client;

    public function __construct()
    {
        $this->client = new Client([
            'base_uri' => $_ENV['THREEDBIN_ENDPOINT'],
        ]);
    }

    /**
     * @throws ThirdPartyApiException
     */
    public function post(string $url, array $data = [], array $options = []): ResponseInterface
    {
        $options['json'] = $data;
        $options['json'][self::QUERY_USERNAME] = $_ENV['THREEDBIN_USERNAME'];
        $options['json'][self::QUERY_API_KEY] = $_ENV['THREEDBIN_API_KEY'];
        try {
            return $this->client->post($url, $options);
        } catch (GuzzleException $guzzleException) {
            throw new ThirdPartyApiException($guzzleException);
        }
    }
}
