<?php

namespace App\ApiClient;

use App\ApiClient\Schema\SmallestBoxSchema;
use App\Exception\NoBinFoundException;
use App\Exception\ThirdPartyApiException;

final class SmallestBoxQuery
{
    private ApiClientInterface $apiClient;
    private BinManagerInterface $binManager;

    public function __construct(ApiClientInterface $apiClient, BinManagerInterface $binManager)
    {
        $this->apiClient = $apiClient;
        $this->binManager = $binManager;
    }

    /**
     * @throws ThirdPartyApiException
     * @throws \JsonException
     * @throws NoBinFoundException
     */
    public function request(SmallestBoxSchema $items): array
    {
        $response = $this->apiClient->post(ApiClient::ENDPOINT_PACK, [
            'bins' => $this->getBins(),
            'items' => $this->prepareItems($items),
        ]);
        $body = $response->getBody()->getContents();

        $parsed = json_decode($body, true, 512, JSON_THROW_ON_ERROR);
        $binsPacked = $parsed['response']['bins_packed'];

        $smallestBin = $this->smallestBin($binsPacked);
        if (null === $smallestBin) {
            throw new NoBinFoundException();
        }

        $binData = $smallestBin['bin_data'];
        return [
            'width' => $binData['w'],
            'height' => $binData['h'],
            'depth' => $binData['d'],
        ];
    }

    private function getBins(): array
    {
        $request = [];
        foreach ($this->binManager->getAllBins() as $bin) {
            $request[] = [
                'id' => $bin->getId(),
                'w' => $bin->getWidth(),
                'h' => $bin->getHeight(),
                'd' => $bin->getDepth(),
                'max_wg' => $bin->getMaxWeight(),
            ];
        }

        return $request;
    }

    private function prepareItems(SmallestBoxSchema $schema): array
    {
        $request = [];
        foreach ($schema->items as $item) {
            $request[] = [
                'id' => $item->id,
                'w' => $item->width,
                'h' => $item->height,
                'd' => $item->depth,
                'wg' => $item->weight,
                'q' => 1,
                'vr' => 1,
            ];
        }

        return $request;
    }

    private function smallestBin($filteredBins): ?array
    {
        $smallestBin = null;
        $smallestVolume = PHP_INT_MAX;

        foreach ($filteredBins as $bin) {
            // filter out boxes that can't fit all of the items
            if (count($bin['not_packed_items']) > 0) {
                continue;
            }

            $binData = $bin['bin_data'];
            $binVolume = (int)$binData['w'] * (int)$binData['h'] * (int)$binData['d'];
            if ($binVolume < $smallestVolume) {
                $smallestBin = $bin;
                $smallestVolume = $binVolume;
            }
        }

        return $smallestBin;
    }
}
