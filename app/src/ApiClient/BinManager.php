<?php


namespace App\ApiClient;


use App\Repository\PackingBinRepository;

class BinManager implements BinManagerInterface
{
    private PackingBinRepository $binRepository;

    public function __construct(PackingBinRepository $binRepository)
    {
        $this->binRepository = $binRepository;
    }

    public function getAllBins(): array
    {
        $allBins = $this->binRepository->findAll();
        if ([] === $allBins) {
            throw new \RuntimeException('No bins specified in database');
        }
        return $allBins;
    }
}
