<?php

namespace App\Entity;

use App\Repository\API3DBinCacheRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=API3DBinCacheRepository::class)
 * @ORM\Table(
 *     indexes={@ORM\Index(name="by_request", columns={"request"})},
 *     uniqueConstraints={@ORM\UniqueConstraint(name="unique_request", columns={"request"})}
 * )
 */
class API3DBinCache
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @ORM\Column(type="text")
     */
    private string $request;

    /**
     * @ORM\Column(type="text")
     */
    private string $response;

    public function __construct(string $request, string $response)
    {
        $this->request = $request;
        $this->response = $response;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRequest(): string
    {
        return $this->request;
    }

    public function setRequest(string $request): self
    {
        $this->request = $request;

        return $this;
    }

    public function getResponse(): string
    {
        return $this->response;
    }

    public function setResponse(string $response): self
    {
        $this->response = $response;

        return $this;
    }
}
