<?php

namespace App\Controller;

use App\ApiClient\Schema\SmallestBoxSchema;
use App\ApiClient\SmallestBoxQuery;
use App\Exception\NoBinFoundException;
use App\Exception\ThirdPartyApiException;
use Swaggest\JsonSchema\Exception;
use Swaggest\JsonSchema\InvalidValue;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class SmallestBoxAction
{
    /**
     * @Route(path="/smallest-box", name="api.smallest_box")
     */
    public function __invoke(Request $request, SmallestBoxQuery $query): Response
    {
        try {
            $data = json_decode($request->getContent(), false, 512, JSON_THROW_ON_ERROR);
        } catch (\JsonException $e) {
            return new JsonResponse([
                'error' => 'Failed to parse client JSON: '.$e->getMessage(),
            ], Response::HTTP_BAD_REQUEST);
        }

        try {
            /** @var SmallestBoxSchema $safeData */
            $safeData = SmallestBoxSchema::import($data);
        } catch (InvalidValue $e) {
            return new JsonResponse([
                'error' => $e->getMessage(),
            ], Response::HTTP_BAD_REQUEST);
        } catch (Exception $e) {
            return new JsonResponse([
                'error' => 'Internal server error. We will look into this issue.',
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        try {
            return new JsonResponse(['box' => $query->request($safeData)]);
        } catch (NoBinFoundException $e) {
            return new JsonResponse([
                'error' => 'No box that could fit all items found',
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        } catch (ThirdPartyApiException | \JsonException $e) {
            return new JsonResponse([
                'error' => 'External API fail',
            ], Response::HTTP_SERVICE_UNAVAILABLE);
        }
    }
}
